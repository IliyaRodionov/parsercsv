<?php
class Parser {
    private $filename;
    public $pdo;                //for __construct()
    public $insertTableGoods;   //for insertTable()
    public $str;                //for insertTable()
    public $counter = 0;        //for insertTable()

    public $strNew;             //for filterStr()
    public $outputArt;          //for filterStr()
    public $outputClearNum;     //for filterStr()

    function __construct($array){
        $dsn    = sprintf('mysql:host=%s:%s;dbname=%s;charset=%s',
            $array['host'],
            $array['port'],
            $array['dbname'],
            $array['charset']
            );
        try {
            $this->pdo = new PDO($dsn, $array['username'], $array['password']);

        } catch (PDOException $e) {

            echo "error!: " . $e->getMessage() . "<br>";
            echo "\n"."Возможно такая база данных не существует или указан ошибочный порт! Обратитесь в службу поддержки!";
            die();

        }
    }

    function parseCsv($filename = 'orders.csv'){

        $this->filename =$filename;

           if($this->pdo) {

               if ($this->copyTable()) {

                   if ($this->deleteTable()) {

                       if ($this->createTable()) {

                           if ($this->insertTable()) {

                                   echo "Парсинг документа успешно завершен, таблицы созданы!";

                           } else echo "Произошла ошибка на этапе добавления данных в таблицу";

                       } else echo "Произошла ошибка на этапе создания таблиц!";

                   } else echo "Произошла ошибка на этапе удаления таблиц!";

               } else echo "Произошла ошибка на этапе создания копий таблиц!";

           } else echo "\n"."Возможно такая база данных не существует или указан ошибочный порт! Обратитесь в службу поддержки!";

        }

        protected function createTable(){
            //Таблица заказов Orders -----------------------------------------------------------------------------------------------
                $createTableOrd = "CREATE TABLE orders (
                    ord_num                         int     (10)  NOT NULL,
                    status                          VARCHAR (100)  NOT NULL,
                    ord_data                        VARCHAR (100)  NOT NULL,
                    full_name                       VARCHAR (100)  NOT NULL,
                    Phone                           VARCHAR (100)  NOT NULL,
                    email                           VARCHAR (100)  NOT NULL,
                    goods                           VARCHAR (255) NOT NULL,
                    total_cost                      int     (20)  NOT NULL,
                    currency                        VARCHAR (100)  NOT NULL,
                    tax                             int     (20)  NOT NULL,
                    value_of_goods                  int     (20)  NOT NULL,
                    benefit                         int     (20)  NOT NULL,
                    payment_method                  VARCHAR (100)  NOT NULL,
                    delivery_method                 VARCHAR (100)  NOT NULL,
                    delivery_address                VARCHAR (100)  NOT NULL,
                    user_comment                    VARCHAR (255) NOT NULL,
                    administrator_comment           VARCHAR (255) NOT NULL,
                    commentary_status               VARCHAR (100)  NOT NULL,
                    paid_for                        VARCHAR (100)  NOT NULL,
                    PRIMARY KEY (ord_num)
    )";
                $createOrd = $this->pdo->prepare($createTableOrd);
                $createOrd->execute();
                $createOrd->closeCursor();

            //Таблица заказов goods ----------------------------------------------------------------------------------------

                $createTableGoods = "CREATE TABLE  goods (
                    ord_num                         INT     (10)  NOT NULL,
                    articul                         VARCHAR (100) NOT NULL,
                    good                            VARCHAR (255) NOT NULL,
                    amount                          INT     (100) NOT NULL

    )";
                $createG = $this->pdo->prepare($createTableGoods);
                $createG->execute();

                return true;
        }

        protected function copyTable(){

        //удаление предыдущих копий таблиц

            $deleteCopyOrd = "DROP TABLE IF EXISTS copy_ord";
            $deleteOrd = $this->pdo->prepare($deleteCopyOrd);
            $deleteOrd->execute();
            $deleteOrd->closeCursor();

            $deleteCopyGoods = "DROP TABLE IF EXISTS copy_good";
            $deleteGood = $this->pdo->prepare($deleteCopyGoods);
            $deleteGood->execute();
            $deleteOrd->closeCursor();

            //создание новых копий таблиц
                $copyTableOrd = "CREATE TABLE copy_ord LIKE orders; 
               
                                 INSERT INTO copy_ord SELECT * FROM orders;";
                $copyOrd = $this->pdo->prepare($copyTableOrd);
                $copyOrd->execute();
                $copyOrd->closeCursor();

                $copyTableGoods = "CREATE TABLE copy_good LIKE goods; 
                                   
                                   INSERT INTO copy_good SELECT * FROM goods;";
                $copyGoods = $this->pdo->prepare($copyTableGoods);
                $copyGoods->execute();
                //$copyGoods->closeCursor();

                return true;
        }

        protected function deleteTable(){

                $deleteTableOrd = "DROP TABLE IF EXISTS orders";
                $deleteOrd = $this->pdo->prepare($deleteTableOrd);
                $deleteOrd->execute();
                $deleteOrd->closeCursor();

                $deleteTableGoods = "DROP TABLE IF EXISTS goods";
                $deleteGood = $this->pdo->prepare($deleteTableGoods);
                $deleteGood->execute();

                return true;

        }

        protected function insertTable(){

            //Чтение данных, для записи в столбцы
            $read = fopen($this->filename, "r");

                while ($this->str = fgetcsv($read, "", ";")) {


                    if($this->counter == 0){
                        $this->counter++;
                        continue;
                    }
                    $this->counter++;


                    echo $this->counter;
                    echo "\r";


                    if(count($this->str) < 19 || count($this->str) > 19) {

                        echo "Строка № ".$this->counter."\n";
                        print_r($this->str);
                        continue;

                    }

                    $insertTableOrders = ("INSERT INTO orders (
                    ord_num,
                    status,
                    ord_data,
                    full_name,
                    Phone,
                    email,
                    goods,
                    total_cost,
                    currency, tax, value_of_goods, benefit,
                    payment_method, delivery_method, delivery_address,
                    user_comment, administrator_comment, commentary_status, paid_for)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    $insertOrd = $this->pdo->prepare($insertTableOrders);
                    $insertOrd->execute($this->str);
                    $insertOrd->closeCursor();

                    $this->insertTableGoods = ("INSERT INTO goods (ord_num, articul, good, amount)
                                         VALUES (?, ?, ?, ?)");
                    $this->filterStr($this->str);

                }   fclose($read);

            return true;
        }

        private function prepareFilterStr($query){

            $insertGood = $this->pdo->prepare($query);
            $insertGood->execute(array($this->str[0], $this->outputArt[0], $this->strNew, $this->outputClearNum[0]));

        }

        protected function filterStr($str){

            $goodsStr = $str[6];
            $arr = explode("],", $goodsStr);

            foreach ($arr as $s){
                $this->strNew = trim($s," []");

                preg_match("/^\d\S*/", $this->strNew, $this->outputArt);
                preg_match_all("/\s-\s\d*шт\./", $this->strNew, $outputNum);

                    if(count($outputNum[0]) > 1) {

                        echo "Ошибка в строке №$this->counter: \n";
                        echo $this->strNew;
                        die();

                    }
                preg_match("/\d\d*/", $outputNum[0][0], $this->outputClearNum);

                $this->prepareFilterStr($this->insertTableGoods);
            }

        }

}